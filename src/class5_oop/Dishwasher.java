package class5_oop;

import javax.naming.directory.InvalidAttributesException;

import static class5_oop.Status.*;

public class Dishwasher {

    private Status status;
    private String[] content;
    private int limit;
    private int currentLoad;

    public Dishwasher(int limit) throws InvalidAttributesException {
        if (limit <= 0) {
            throw new InvalidAttributesException();
        }
        this.limit = limit;
        this.status = READY_TO_USE;
        this.content = new String[limit];
    }

    public Status getStatus() {
        return status;
    }

    /**
     * This method prints the dishwasher content
     * null array element ignored
     */
    public void getContent() {
        for (int i = 0; i < limit; i++) {
            if (content[i] != null) {
                System.out.println(content[i]);
            }
        }
    }

    /**
     * Add dishes to the dishwasher
     * The dishwasher has a limit
     * If dishes cannot be added due to a wrong dishwasher state, WrongStatusException is thrown
     * If dishes cannot be added due to the load limit, OutOfLimitException is thrown
     *
     * @param dish
     */
    public void addDishes(String dish) throws WrongStatusException, OutOfLimitException {
        if (status.equals(IN_PROGRESS) || status.equals(CLEAN)) {
            throw new WrongStatusException();
        } else {
            if (currentLoad < limit) {
                content[currentLoad] = dish;
                currentLoad++;
                status = DURTY;
            } else {
                throw new OutOfLimitException();
            }
        }
    }

    /**
     * Start the dishwasher
     * If  the dishwasher is still busy with clean dishes, WrongStatusException is thrown
     * The dishwasher cannot be started if it's empty
     */
    public void start() throws WrongStatusException, ZeroLoadException {
        if (status.equals(CLEAN)) {
            throw new WrongStatusException();
        } else if (currentLoad == 0) {
            throw new ZeroLoadException();
        } else {
            status = IN_PROGRESS;
        }
    }

    /**
     * Stop the dishwasher
     */
    public void stop() {
        status = CLEAN;
    }

    /**
     * Remove dishes from the dishwasher
     */
    public void empty() {
        for (int i = 0; i < content.length; i++) {
            content[i] = null;
            currentLoad = 0;
        }
        status = READY_TO_USE;
    }

}
