package class5_oop;

import javax.naming.directory.InvalidAttributesException;

public class DishwasherExe {

    public static void main(String[] args) throws WrongStatusException, OutOfLimitException, ZeroLoadException {
        Dishwasher dishwasher = null;
        try {
            dishwasher = new Dishwasher(3);
        } catch (InvalidAttributesException e) {
            e.printStackTrace();
        }

        dishwasher.addDishes("plate");
        dishwasher.addDishes("plate");
        dishwasher.getContent();
        dishwasher.addDishes("spoon");
        dishwasher.addDishes("mug");
        dishwasher.getContent();
        System.out.println(dishwasher.getStatus());

        dishwasher.start();
        System.out.println(dishwasher.getStatus());
        dishwasher.getStatus();
        dishwasher.addDishes("plate");
        dishwasher.stop();
        System.out.println(dishwasher.getStatus());

        dishwasher.addDishes("plate");
        dishwasher.empty();
        dishwasher.getContent();
        dishwasher.addDishes("plate");
        dishwasher.getContent();
        System.out.println(1);
    }
}
