package class5_oop;

/**
 * This exception is thrown when the dishwasher load limit is attempted to be overcome
 */
public class OutOfLimitException extends Exception {
    public OutOfLimitException() {
        super("New dishes cannot be added, the dishwasher limit is already reached");
    }
}
