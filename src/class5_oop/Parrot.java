package class5_oop;

public class Parrot {

    private String currentPhrase = "Squak!";

    public String getCurrentPhrase() {
        return currentPhrase;
    }

    public void setCurrentPhrase(String currentPhrase) {
        this.currentPhrase = currentPhrase;
    }

    public void speak(){
        System.out.println(getCurrentPhrase());
    }

    public void teach(String phrase){
        setCurrentPhrase(phrase);
    }
}
