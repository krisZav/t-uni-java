package class5_oop;

public class ParrotExe {

    public static void main(String[] args) {
        Parrot polly = new Parrot();
        polly.speak();
        polly.teach("Polly wants a cracker");
        polly.speak();
        polly.speak();
        polly.teach("I'm a parrot");
        polly.speak();
    }
}
