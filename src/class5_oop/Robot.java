package class5_oop;

public class Robot {

    private int x;
    private int y;
    private String direction = "up";

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getPosition() {
        return "(" + getX() + ", " + getY() + ")";
    }

    public void forward() {
        switch(getDirection()) {
            case "up":
                setY(getY()+1);
                break;
            case "down":
                setY(getY()-1);
                break;
            case "left":
                setX(getX()-1);
                break;
            case "right":
                setX(getX()+1);
                break;
        }
    }

    public void backward() {
        switch(getDirection()) {
            case "up":
                setY(getY()-1);
                break;
            case "down":
                setY(getY()+1);
                break;
            case "left":
                setX(getX()+1);
                break;
            case "right":
                setX(getX()-1);
                break;
        }
    }


    public void rotateLeft() {
        switch(getDirection()) {
            case "up":
                setDirection("left");
                break;
            case "down":
                setDirection("right");
                break;
            case "left":
                setDirection("down");
                break;
            case "right":
                setDirection("up");
                break;
        }
    }

    public void rotateRight() {
        switch(getDirection()) {
            case "up":
                setDirection("right");
                break;
            case "down":
                setDirection("left");
                break;
            case "left":
                setDirection("up");
                break;
            case "right":
                setDirection("down");
                break;
        }
    }
}
