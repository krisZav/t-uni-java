package class5_oop;

public class RobotExe {
    public static void main(String[] args) {
        Robot robot = new Robot();
        robot.getPosition();
        robot.getDirection();
        robot.forward();
        robot.forward();
        robot.rotateLeft();
        robot.forward();
        robot.forward();
        robot.rotateRight();
        robot.backward();
        robot.getPosition();
        System.out.println(robot.getPosition());
        robot.rotateRight();
        robot.getDirection();
        System.out.println(robot.getDirection());
    }
}
