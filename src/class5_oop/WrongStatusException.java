package class5_oop;

/*
 *This exception is thrown when some action cannot be applied to the object while it's in an inappropriate status
 */
public class WrongStatusException extends Exception {

    public WrongStatusException() {
        super("Action cannot be applied to the object due to its current status");
    }
}
