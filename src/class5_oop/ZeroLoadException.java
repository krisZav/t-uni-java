package class5_oop;

/**
 * This exception is thrown if some action is getting applied to an empty dishwasher
 */
public class ZeroLoadException extends Exception {
    public ZeroLoadException() {
        super("The dishwasher is empty");
    }
}
