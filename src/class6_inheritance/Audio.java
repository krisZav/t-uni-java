package class6_inheritance;

import java.util.Date;

public class Audio extends MediaMessage implements Playable {

    private int duration;
    private String audioType;
    private Object audioData;

    public Audio(String author, Date date, String message, MessageType messageType, String fileName, String extension) {
        super(author, date, message, messageType, fileName, extension);
    }

    @Override
    public void play() {
        System.out.println("Audio is getting played:" + super.fileName);
    }

    //i don't know which type ans audioFile will have, therefore I used Object
    public Object normalaze() {
        return new Object();
    }
}
