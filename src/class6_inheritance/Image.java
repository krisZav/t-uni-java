package class6_inheritance;

import java.util.Date;

public class Image extends MediaMessage implements Printable {

    private String resolution;
    private Object image;

    public Image(String author, Date date, String message, MessageType messageType, String fileName, String extension) {
        super(author, date, message, messageType, fileName, extension);
    }

    @Override
    public void print() {
        System.out.println("Print image title: " + super.fileName);
    }

    public Object chanageResolution(int x, int y) {
        return new Object();
    }
}
