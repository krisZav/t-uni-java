package class6_inheritance;

import java.util.Date;

public class MediaMessage extends Message {

    private String extension;
    protected String fileName;
    private int maxSize = 20;

    public MediaMessage(String author, Date date, String message, MessageType messageType, String fileName, String extension) {
        super(author, date, message, messageType);
        this.fileName = fileName;
        this.extension = extension;
    }

    @Override
    public void displayContent() {
        System.out.println(super.buildMessageContent() + ", " + extension);
    }

    public String getFileName() {
        return fileName + "." + extension;
    }

    public int getMaxSize() {
        return maxSize;
    }
}
