package class6_inheritance;

import java.util.Date;

abstract public class Message {

    protected String author;
    protected Date date;
    protected String message;
    protected MessageType messageType;

    public Message(String author, Date date, String message, MessageType messageType) {
        this.author = author;
        this.date = date;
        this.message = message;
        this.messageType = messageType;
    }

    protected String buildMessageContent() {
        return author + ", " + date + ", " + message + ", " + messageType;
    }

    public void displayContent() {
        System.out.println(buildMessageContent());
    }

    abstract public int getMaxSize();
}
