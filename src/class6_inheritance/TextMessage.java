package class6_inheritance;

import java.util.Date;

public class TextMessage extends Message implements Printable {

    private String language;
    private String title;
    private int maxSize = 2;

    public TextMessage(String author, Date date, String message, MessageType messageType, String title, String language) {
        super(author, date, message, messageType);
        this.title = title;
        this.language = language;
    }

    @Override
    public void displayContent() {
        System.out.println(super.buildMessageContent() + ", " + language);
    }

    @Override
    public int getMaxSize() {
        return maxSize;
    }

    @Override
    public void print() {
        System.out.println("Print text message:" + title);
    }

    public void replaceAbusiveWords() {
        message = super.message.replace("idiot", "id**");
    }
}
