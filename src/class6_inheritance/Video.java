package class6_inheritance;

import java.util.Date;

public class Video extends MediaMessage implements Playable {

    private int bitRate;
    private Object video;

    public Video(String author, Date date, String message, MessageType messageType, String fileName, String extension) {
        super(author, date, message, messageType, fileName, extension);
    }

    @Override
    public void play() {
        System.out.println("Video is getting played: " + super.fileName);
    }

    public Object speedUp(int targetBitRate) {
        return new Object();
    }
}
