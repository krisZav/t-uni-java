package class7_collections;

import java.util.*;

public class Iterations {

    public static void iterateListFor(List<Student> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.println(i + ": " + list.get(i));
        }
    }


    public static void iterateListForEach(List<Student> list) {
        for (Student student : list
                ) {
            System.out.println(student);
        }
    }

    public static void iterateListWhile(List<Student> list) {
        int i = 0;
        while (i < list.size()) {
            System.out.println(i + ": " + list.get(i));
            i++;
        }
    }

    public static void iterateSetFor(Set<Student> set) {
        Iterator it = set.iterator();
        for (; it.hasNext(); ) {
            System.out.println(it.next());
        }
    }

    public static void iterateSetForEach(Set<Student> set) {
        for (Student student : set) {
            System.out.println(student);
        }
    }

    public static void iterateSetWhile(Set<Student> set) {
        Iterator it = set.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }

    public static void iterateMapFor(Map<Integer, Student> map) {
        List<Student> students = new ArrayList<>(map.values());
        for (int i = 0; i < students.size(); i++) {
            System.out.println(students.get(i));
        }
    }

    public static void iterateMapForEachByValue(Map<Integer, Student> map) {
        Collection<Student> students = map.values();
        for (Student student : students) {
            System.out.println(student);
        }
    }

    public static void iterateMapWhile(Map<Integer, Student> map) {
        List<Student> students = new ArrayList<>(map.values());
        int i = 0;
        while (i < students.size()) {
            System.out.println(students.get(i));
            i++;
        }
    }

    public static void iterateMapForEachByKey(Map<Integer, Student> map) {
        Set<Integer> keys = map.keySet();
        for (Integer key : keys) {
            System.out.println(key + ": " + map.get(key));
        }
    }

}
