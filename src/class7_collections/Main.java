package class7_collections;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Student s1 = new Student(1, "Irina");
        Student s2 = new Student(2, "Sergey");
        Student s3 = new Student(3, "Jana");

        List<Student> list1 = new ArrayList<>();
        list1.add(s1);
        list1.add(s2);
        list1.add(s2);
        list1.add(s3);
        List<Student> list2 = new ArrayList<>();
        list2.add(s2);
        list2.add(s3);
        List<Student> students = StudentsList.returnList(list1, list2);

        Set<Student> set1 = new HashSet<>();
        set1.add(s1);
        set1.add(s2);
        set1.add(s2);
        set1.add(s3);
        Set<Student> set2 = new HashSet<>();
        set2.add(s2);
        set2.add(s3);
        Set<Student> students2 = StudentsSet.returnSet(set1, set2);

        Iterations.iterateListFor(list1);
        Iterations.iterateListForEach(list1);
        Iterations.iterateListWhile(list1);
        System.out.println();
        Iterations.iterateSetFor(set1);
        System.out.println();
        Iterations.iterateSetForEach(set1);
        System.out.println();
        Iterations.iterateSetWhile(set1);
        System.out.println("map");

        Map<Integer, Student> map = new HashMap<>();
        map.put(s1.getId(), s1);
        map.put(s2.getId(), s2);
        map.put(s3.getId(), s3);
        Iterations.iterateMapForEachByKey(map);
        System.out.println();
        Iterations.iterateMapForEachByValue(map);
        System.out.println();
        Iterations.iterateMapFor(map);
        System.out.println();
        Iterations.iterateMapWhile(map);
    }
}
