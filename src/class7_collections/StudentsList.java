package class7_collections;

import java.util.ArrayList;
import java.util.List;

public class StudentsList {

    public static List<Student> returnList(List<Student> studentsList1, List<Student> studentsList2) {
        List<Student> equalStudents = new ArrayList<>();
        for (Student student1 : studentsList1
                ) {
            for (Student student2 : studentsList2
                    ) {
                if (student1.equals(student2)) {
                    equalStudents.add(student1);
                }
            }
        }
        return equalStudents;
    }
}
