package class7_collections;

import java.util.HashSet;
import java.util.Set;

public class StudentsSet {

    public static Set<Student> returnSet(Set<Student> studentsSet1, Set<Student> studentsSet2) {
        Set<Student> equalStudents = new HashSet<>();
        for (Student student1 : studentsSet1) {
            for (Student student2 : studentsSet2
                    ) {
                if (student1.equals(student2)) {
                    equalStudents.add(student1);
                }
            }
        }
        return equalStudents;
    }
}
